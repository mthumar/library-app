'use strict';

angular.module('libraryAppApp')
  .controller('MainCtrl', function ($scope, Auth) {
    Auth.logout();
  });
