'use strict';

angular.module('libraryAppApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/home', {
        templateUrl: 'app/books/books.html',
        controller: 'BooksCtrl',
        authenticate: true
      })
      .when('/bookDetails/:bookId', {
        templateUrl: 'app/books/bookDetails.html',
        controller: 'BookDetailsCtrl',
        authenticate: true
      })
      .when('/newbook', {
        templateUrl: 'app/books/booksAddUpdate.html',
        controller: 'BooksAddUpdateCtrl',
        authenticate: true
      })
      .when('/book/:bookId', {
        templateUrl: 'app/books/booksAddUpdate.html',
        controller: 'BooksAddUpdateCtrl',
        authenticate: true
      });
  });