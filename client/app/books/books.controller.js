'use strict';

angular.module('libraryAppApp')
  .controller('BooksCtrl', function ($scope, $http, socket) {
    $scope.booksList = [];
    $scope.loading = true;
    $scope.tabularView = false;

    $http.get('/api/books').success(function(booksList) {
      $scope.booksList = booksList;
      $scope.loading = false;
      socket.syncUpdates('book', $scope.booksList);
    });

    $scope.deleteBook = function(book) {
      $http.delete('/api/books/' + book._id);
    };

    $scope.$on('$destroy', function () {
      socket.unsyncUpdates('book');
    });
    
    $scope.changeView = function (view) {
        $scope.tabularView = view;
    };
    
  });
