'use strict';

angular.module('libraryAppApp')
  .controller('BookDetailsCtrl', function ($scope, $http, socket, $timeout, $routeParams, $location) {
    $scope.book = {};
    $scope.error = false;

    $scope.bookId = $routeParams.bookId;
    
    if($scope.bookId) {
      $http.get('/api/books/'+$routeParams.bookId)
        .success(function(booksDetails) {
          $scope.book = booksDetails;
        })
        .error(function(){
          $scope.error = true;    
        });
    }
    else {
      $scope.error = true;  
    }

    $scope.deleteBook = function(book) {
      $http.delete('/api/books/' + book._id).success(function(){
        $location.path('/home');
      })
      .error(function(){
        $scope.error = true;
      });
    };

  });
