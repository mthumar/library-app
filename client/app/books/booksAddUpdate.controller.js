'use strict';

angular.module('libraryAppApp')
  .controller('BooksAddUpdateCtrl', function ($scope, $http, socket, $timeout, $routeParams, $location) {
    $scope.successMessage = '';
    $scope.book = {};
    $scope.bookId = $routeParams.bookId;
    
    if($scope.bookId) {
      $http.get('/api/books/'+$routeParams.bookId).success(function(booksDetails) {
        $scope.book = booksDetails;
      });
    }
    
    $scope.save = function() {
      var url = $scope.bookId ? '/api/books/' + $scope.bookId : '/api/books'; 
      $http.post(url, $scope.book).success(function(){
        $location.path('/home');
      }).error(function(){
        $scope.successMessage = 'There was an error :(, Please try again later';
        $timeout(function(){
            $scope.successMessage = '';  
        }, 7000);
      });
    };

  });
