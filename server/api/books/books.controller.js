/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /books              ->  index
 * POST    /books              ->  create
 * GET     /books/:id          ->  show
 * PUT     /books/:id          ->  update
 * DELETE  /books/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var Books = require('./books.model');

// Get list of books
exports.index = function(req, res) {

  var query = Books.find({}).select('name author about');

    query.exec(function (err, someValue) {
       if(err) { return handleError(res, err); }
        return res.json(200, someValue);
    });
  
};

// Get a single Book
exports.show = function(req, res) {
  Books.findById(req.params.id, function (err, book) {
    if(err) { return handleError(res, err); }
    if(!book) { return res.send(404); }
    return res.json(book);
  });
};

// Add a new book in the DB.
exports.create = function(req, res) {
  Books.create(req.body, function(err, book) {
    if(err) { return handleError(res, err); }
    return res.json(201, book);
  });
};

// Updates an existing book in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Books.findById(req.params.id, function (err, book) {
    if (err) { return handleError(res, err); }
    if(!book) { return res.send(404); }
    var updated = _.merge(book, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, book);
    });
  });
};

// Deletes a book from the DB.
exports.destroy = function(req, res) {
  Books.findById(req.params.id, function (err, book) {
    if(err) { return handleError(res, err); }
    if(!book) { return res.send(404); }
    book.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}