'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var BookSchema = new Schema({
  name: String,
  edition: String,
  author: String,
  about: String,
  isPublished: Boolean,
  lastUpdated: { type: Date, default: Date.now },
  numberOfPages: Number
});

module.exports = mongoose.model('Book', BookSchema);