# Library App #

Sample Angular + Mongo application scaffolded from yo angular-fullstack

* [Learn More about yo angular-fullstack](https://github.com/DaftMonk/generator-angular-fullstack#usage)

### How do I get set up? ###

* Clone the repo. using ``` git clone ... ```
* Run ```npm install``` and ``` bower install ``` to download the dependencies
* install mongodb and run it as ```mongod```
* Start the application with ``` grunt serve ```
That's it! :)


### Who do I talk to? ###

* do drop in a mailto thumarmahesh@gmail.com